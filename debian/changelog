r-cran-sn (2.1.1-2) unstable; urgency=medium

  * Rebuilding for unstable following bookworm release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 22 Jun 2023 20:44:05 -0500

r-cran-sn (2.1.1-1) experimental; urgency=medium

  * New upstream release (into 'experimental' while Debian is frozen)

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Switch to virtual debhelper-compat (= 12)

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 14 Apr 2023 20:24:58 -0500

r-cran-sn (2.1.0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version  

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 11 Aug 2022 06:36:24 -0500

r-cran-sn (2.0.2-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 10 Mar 2022 11:32:49 -0600

r-cran-sn (2.0.1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version  

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 28 Nov 2021 15:01:24 -0600

r-cran-sn (2.0.0-2) unstable; urgency=medium

  * Simple rebuild for unstable following Debian 11 release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 Aug 2021 17:19:04 -0500

r-cran-sn (2.0.0-1) experimental; urgency=medium

  * New upstream release (to experimental during freeze)

  * debian/control: Set Build-Depends: to recent R version
  * debian/control: Set Standards-Version: to current version  
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 24 Apr 2021 17:49:36 -0500

r-cran-sn (1.6-2-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 02 Jun 2020 14:20:36 -0500

r-cran-sn (1.6-1-3) unstable; urgency=medium

  * Rebuilt for r-4.0 transition

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 18 May 2020 10:27:14 -0500

r-cran-sn (1.6-1-2) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

  * debian/control: Add (Build-)Depends on r-cran-quantreg

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 02 Apr 2020 08:12:48 -0500

r-cran-sn (1.5-5-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 02 Feb 2020 11:20:44 -0600

r-cran-sn (1.5-4-2) unstable; urgency=medium

  * Source-only upload
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 17 Aug 2019 17:47:13 -0500

r-cran-sn (1.5-4-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 15 May 2019 20:53:44 -0500

r-cran-sn (1.5-3-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

  * debian/control: Correct Architecture: to 'all' [lintian]

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 12 Nov 2018 09:43:23 -0600

r-cran-sn (1.5-2-2) unstable; urgency=medium

  * Rebuilding to correct an issue created by dh-r 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 15 Jul 2018 15:38:51 -0500

r-cran-sn (1.5-2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 30 Apr 2018 17:54:18 -0500

r-cran-sn (1.5-1-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/compat: Set level to 9

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 03 Dec 2017 18:53:22 -0600

r-cran-sn (1.5-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Add ${misc:Depends} to Depends

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 15 Feb 2017 15:14:26 -0600

r-cran-sn (1.4-0-1) unstable; urgency=medium

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 15 Jul 2016 07:18:00 -0500

r-cran-sn (1.3-0-2) unstable; urgency=medium

  * debian/compat: Created 			(Closes: #827807)
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 21 Jun 2016 05:18:56 -0500

r-cran-sn (1.3-0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 11 Nov 2015 06:23:37 -0600

r-cran-sn (1.2-4-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 30 Aug 2015 11:05:18 -0500

r-cran-sn (1.2-3-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 21 Jul 2015 06:56:29 -0500

r-cran-sn (1.2-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 05 Jun 2015 19:07:29 -0500

r-cran-sn (1.2-1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 30 Apr 2015 07:33:02 -0500

r-cran-sn (1.2-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 28 Mar 2015 08:49:32 -0500

r-cran-sn (1.1-2-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 01 Dec 2014 13:33:52 -0600

r-cran-sn (1.1-1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 30 Oct 2014 11:51:54 -0500

r-cran-sn (1.1-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 04 Aug 2014 17:19:07 -0500

r-cran-sn (1.0-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Add (Build-)Depends on r-cran-numderiv nomnormt (>= 1.3-1)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 14 Jan 2014 06:28:00 -0600

r-cran-sn (0.4-18-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 01 May 2013 19:30:57 -0500

r-cran-sn (0.4-17-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 
  
  * (Re-)building with R 3.0.0 (beta)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 31 Mar 2013 10:23:46 -0500

r-cran-sn (0.4-17-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 23 Jul 2011 09:56:46 -0500

r-cran-sn (0.4-16-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 30 Aug 2010 17:09:33 -0500

r-cran-sn (0.4-15-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 03 Apr 2010 08:25:45 -0500

r-cran-sn (0.4-14-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 17 Jan 2010 13:09:38 -0600

r-cran-sn (0.4-12-2) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  
 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 02 Nov 2009 21:16:51 -0600

r-cran-sn (0.4-12-1) unstable; urgency=low

  * New upstream release

  * debian/control: Changed Section: to new section 'gnu-r'

  * debian/control: Set Standards-Version: to current version 3.8.1
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 22 Mar 2009 21:03:54 -0500

r-cran-sn (0.4-11-2) unstable; urgency=low

  * debian/control: Tighten Build-Depends on mnormt (>= 1.3-1)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 02 Feb 2009 21:49:49 -0600

r-cran-sn (0.4-11-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 30 Jan 2009 18:29:05 -0600

r-cran-sn (0.4-10-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version 2.8.1

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 23 Dec 2008 13:11:31 -0600

r-cran-sn (0.4-8-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version 2.8.0
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 19 Nov 2008 19:47:30 -0600

r-cran-sn (0.4-6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 16 Sep 2008 08:44:37 -0500

r-cran-sn (0.4-5-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version 2.7.1
  * debian/control: Set Standards-Version: to current version 3.8.0
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 02 Aug 2008 16:22:11 -0500

r-cran-sn (0.4-4-1) unstable; urgency=low

  * Initial Debian release
  
 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 12 Oct 2007 20:26:38 -0500
